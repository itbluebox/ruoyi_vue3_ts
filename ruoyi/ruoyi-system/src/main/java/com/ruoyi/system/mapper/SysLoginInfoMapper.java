package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysLoginInfo;

import java.util.List;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author ruoyi
 */
public interface SysLoginInfoMapper {
    /**
     * 新增系统登录日志
     *
     * @param info 访问日志对象
     */
    void insertLoginInfo(SysLoginInfo info);

    /**
     * 查询系统登录日志集合
     *
     * @param sysLoginInfo 访问日志对象
     * @return 登录记录集合
     */
    List<SysLoginInfo> selectLoginInfoList(SysLoginInfo sysLoginInfo);

    /**
     * 批量删除系统登录日志
     *
     * @param infoIds 需要删除的登录日志ID
     * @return 结果
     */
    int deleteLoginInfoByIds(Long[] infoIds);

    /**
     * 清空系统登录日志
     *
     * @return 结果
     */
    int cleanLoginInfo();

    String selectLastLoginByUserName(SysLoginInfo loginInfo);
}
